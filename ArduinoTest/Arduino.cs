﻿using System;
using System.IO.Ports;
using System.Threading;

namespace ArduinoTest {
    public static class Arduino {
        #region COMMUNICATION
        #region CONNECTION ESTABILISHING
        private static SerialPort arduinoSerial = null;
        private const int BAUD_RATE = 115200;

        public static bool connected() {
            return arduinoSerial != null;
        }

        public static bool connect() {
            SerialPort portToCheck = new SerialPort();
            portToCheck.BaudRate = BAUD_RATE;
            foreach (string PORT in SerialPort.GetPortNames()) {
                //foreach (string PORT in new string[] { "COM3" }) {
                Console.WriteLine("TRY CONNECT : " + PORT);
                portToCheck.PortName = PORT;
                try {
                    portToCheck.Open();
                } catch (Exception ex) {
                    Console.WriteLine("EX MSG: "+ex.Message);
                    //continue;
                }

                if (portToCheck.IsOpen) {
                    // check if connected with wanted device
                    if (sendCommand("W2789$", portToCheck, true).Equals("wlf$")) {
                        portToCheck.Close();
                        // create static instance
                        arduinoSerial = new SerialPort(PORT, BAUD_RATE);
                        Console.WriteLine("CONNECTED");
                        return true;
                    }
                    portToCheck.Close();
                }
            }
            return false;
        }

        public static void disconnect() {
            if (arduinoSerial != null) {
                arduinoSerial.Close();
                arduinoSerial.Dispose();
            }
        }

        public static string getPort() {
            return connected() ? arduinoSerial.PortName : "NONE";
        }
        #endregion // CONNECTION ESTABILISHING

        #region COMMAND HANDLING
        public static string sendCommand(string command) {
            return sendCommand(command, arduinoSerial);
        }

        public static string sendCommand(string command, SerialPort portToSend, bool closeConnection = false) {
            if (portToSend != null) {
                // connect with port
                if (!portToSend.IsOpen)
                    try {
                        portToSend.Open();
                    } catch (Exception ex) {
                        return "";
                    }

                // send command
                portToSend.Write(command);
                Thread.Sleep(120);

                // read response
                int count = portToSend.BytesToRead;
                string response = "";
                while (count-- > 0)
                    response += Convert.ToChar(portToSend.ReadByte());
                
                // close connection
                if (closeConnection)
                    portToSend.Close();

                return response.Replace("\r\n","");
            }
            return "";
        }
        #endregion // COMMAND HANDLING
        #endregion // CONNECTION

        #region MODULES
        public static string readClock() {
            return sendCommand("CLOCK$");
        }

        public static string readGY521() {
            return sendCommand("GY521$");
        }
        #endregion // MODULES
    }
}

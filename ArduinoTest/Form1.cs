﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.Drawing.Drawing2D;

namespace ArduinoTest {
    public partial class Form1 : Form {

        public Form1() {
            InitializeComponent();

            timer.Tick += Timer_Tick;
            drawingPanel.Paint += DrawingPanel_Paint;

            while (! Arduino.connected()) {
                Arduino.connect();
            }
        }

        private void Form1_Load(object sender, EventArgs e) {
            drawingPanel.Image = new Bitmap(drawingPanel.Width, drawingPanel.Height);

            timer.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            Arduino.disconnect();
        }

        
        private void Timer_Tick(object sender, EventArgs e) {
            Gyroscope.read();
            Cursor.Position = new Point((int)(Cursor.Position.X + Gyroscope.yaw * -10), (int)(Cursor.Position.Y + Gyroscope.pitch * 10));
            drawingPanel.Refresh();
        }

        private void DrawingPanel_Paint(object sender, PaintEventArgs e) {
            e.Graphics.FillRectangle(Brushes.White, drawingPanel.ClientRectangle);

            Matrix savedMatrix = e.Graphics.Transform;

            Pen pen = new Pen(Brushes.Black, 2);
            e.Graphics.DrawString(Arduino.readClock(), new Font(FontFamily.GenericMonospace, 20), pen.Brush, 10, 10);

            e.Graphics.TranslateTransform(drawingPanel.Width/2, drawingPanel.Height/2);

            pen.Color = Color.Red;
            e.Graphics.DrawLine(pen, -15, 0, 15, 0);
            e.Graphics.DrawLine(pen, 0, -15, 0, 15);

            e.Graphics.TranslateTransform(Gyroscope.yaw*-100, Gyroscope.pitch * 100);

            pen.Color = Color.Blue;
            e.Graphics.FillEllipse(pen.Brush, -5, -5, 10, 10);


            //pen.CustomEndCap = new AdjustableArrowCap(5,5);
            //e.Graphics.DrawLine(pen, 0, 0, 25, 0);

            e.Graphics.Transform = savedMatrix;
        }

    }
}

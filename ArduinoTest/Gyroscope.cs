﻿using System;

namespace ArduinoTest {
    public static class Gyroscope {
        public static float yaw = 0, pitch = 0, roll = 0;

        public static void read() {
            string data;

            data = Arduino.readGY521();
            if (data.Length > 0) {
                string[] fields = data.Replace('.', ',').Split(';');
                if (fields.Length == 3) {
                    yaw = float.Parse(fields[0]);
                    pitch = float.Parse(fields[1]);
                    roll = float.Parse(fields[2]);
                }
            }
        }
    }
}
